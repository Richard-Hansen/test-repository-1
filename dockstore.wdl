task hello {
  String name

  command {
    echo 'hello there ${name}!'
  }
  output {
    File response = stdout()
  }
}

workflow test {
  call hello
}